package com.endava.controller;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.entity.Department;
import com.endava.repository.jpa.DepartmentJpaRepository;
import com.endava.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/department")
public class DepartmentRestController {

    private final DepartmentService service;

    @GetMapping
    @ResponseStatus(OK)
    public List<DepartmentDTO> findAll(){
        return service.findAllDepartments();
    }

    @GetMapping("/{name}")
    @ResponseStatus(OK)
    public DepartmentDTO findByName(@PathVariable("name") String name){
        return service.findDepartmentDTOByName(name);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public DepartmentDTO save(@Valid @RequestBody DepartmentDTO departmentDTO){
        return service.saveDepartment(departmentDTO);
    }

    @PutMapping
    @ResponseStatus(OK)
    public DepartmentDTO update(@Valid @RequestBody DepartmentDTO departmentDTO){
        return service.updateDepartment(departmentDTO);
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(NO_CONTENT)
    public void deleteByName(@PathVariable("name") String name){
        service.deleteDepartmentByName(name);
    }
}
