package com.endava.controller;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.dto.EmployeeDTO;
import com.endava.model.entity.Employee;
import com.endava.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/employee")
public class EmployeeRestController {

    private final EmployeeService service;
    
    @GetMapping
    @ResponseStatus(OK)
    public List<EmployeeDTO> findAllEmployees(){
        return service.findAll();
    }

    @GetMapping("/{email}")
    @ResponseStatus(OK)
    public EmployeeDTO findEmployeeByEmail(@PathVariable("email") String email){
        return service.findEmployeeDTOByEmail(email);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public EmployeeDTO save(@Valid @RequestBody EmployeeDTO employeeDTO){
        return service.save(employeeDTO);
    }

    @PutMapping
    @ResponseStatus(OK)
    public EmployeeDTO update(@Valid @RequestBody EmployeeDTO employeeDTO){
        return service.update(employeeDTO);
    }

    @DeleteMapping("/{email}")
    @ResponseStatus(NO_CONTENT)
    public void deleteEmployee(@PathVariable("email") String email){
        service.deleteEmployeeByEmail(email);
    }
}
