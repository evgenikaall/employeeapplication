package com.endava.util.exception;

public class DepartmentsManagingException extends RuntimeException{
    public DepartmentsManagingException() {
    }

    public DepartmentsManagingException(String message) {
        super(message);
    }
}
