package com.endava.repository.jpa;

import com.endava.model.entity.Employee;
import com.endava.repository.CustomRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Profile(value = "jpa")
public interface EmployeeJpaRepository extends JpaRepository<Employee, Long>, CustomRepository<Employee, Long> {
    Optional<Employee> findEmployeeByEmail(String email);
}
