package com.endava.repository.jpa;

import com.endava.model.entity.Department;
import com.endava.repository.CustomRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Profile(value = "jpa")
public interface DepartmentJpaRepository extends JpaRepository<Department, Long>, CustomRepository<Department, Long> {
    Optional<Department> findDepartmentByName(String name);
    Optional<Department> findDepartmentByManagerEmail(String email);
}
