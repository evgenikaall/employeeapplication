package com.endava.repository.hibernate;


import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile(value = "hibernate")
public class DepartmentHibernateRepository {
}
