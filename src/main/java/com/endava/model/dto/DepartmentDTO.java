package com.endava.model.dto;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class DepartmentDTO {

    @NotBlank
    @NotEmpty
    private String name;

    @Email
    @NotNull
    @NotBlank
    private String managerEmail;
}
