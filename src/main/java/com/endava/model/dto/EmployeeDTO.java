package com.endava.model.dto;

import com.endava.model.entity.Department;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;

import static javax.persistence.CascadeType.ALL;

@Data
@Builder
public class EmployeeDTO {

    @NotBlank
    @NotEmpty
    private String firstName;

    @NotBlank
    @NotEmpty
    @NotNull
    private String lastName;

    // can be described via enumerated, but haven't full list of departments and possible jobs
    @NotBlank
    private String job;

    @Email
    @NotNull
    @NotBlank
    private String email;

    @Min(1)
    private Double salary;

    @Column(name = "employee_hire_date")
    private Timestamp hireDate;

    @OneToOne(mappedBy = "manager")
    private DepartmentDTO managedDepartment;

    @ManyToOne(cascade = ALL)
    @JoinColumn(name = "department_id")
    private DepartmentDTO department;
}
