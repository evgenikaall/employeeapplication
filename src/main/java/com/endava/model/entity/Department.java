package com.endava.model.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "department")
public class Department {

    @Id
    @SequenceGenerator(name = "dep_seq", sequenceName = "dep_seq", allocationSize = 1, initialValue = 10)
    @GeneratedValue(strategy = AUTO, generator = "dep_seq")
    @Column(name = "department_id")
    private Long id;

    @NotBlank
    @NotEmpty
    @Column(name = "department_name", unique = true)
    private String name;

    @OneToOne(cascade = ALL)
    @JoinColumn(name = "manager_id", referencedColumnName = "employee_id")
    private Employee manager;


    @OneToMany(mappedBy = "department")
    private Set<Employee> employeeSet = new HashSet<>();

    @PreRemove
    private void removeEmployeeDepartmentRelation(){
        employeeSet.forEach(employee -> employee.setDepartment(null));
    }


}
