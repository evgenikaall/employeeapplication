package com.endava.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;

import static javax.persistence.CascadeType.*;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee")
public class Employee {

    @Id
    @Column(name = "employee_id")
    @SequenceGenerator(name = "emp_seq", sequenceName = "emp_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = AUTO, generator = "emp_seq")
    private Long id;

    @NotBlank
    @NotEmpty
    @Column(name = "employee_first_name")
    private String firstName;

    @NotBlank
    @NotEmpty
    @NotNull
    @Column(name = "employee_last_name")
    private String lastName;

    // can be described via enumerated, but haven't full list of departments and possible jobs
    @NotBlank
    @Column(name = "employee_job")
    private String job;

    @Email
    @NotNull
    @Column(name = "employee_email", unique = true)
    private String email;

    @Min(1)
    @Column(name = "employee_salary")
    private Double salary;

    @Column(name = "employee_hire_date")
    private Timestamp hireDate;

    //TODO ORPHAN REMOVE
    @OneToOne(mappedBy = "manager")
    private Department managedDepartment;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @PreRemove
    private void removeEmployeeManagingRelation(){
        managedDepartment.setManager(null);
    }



}
