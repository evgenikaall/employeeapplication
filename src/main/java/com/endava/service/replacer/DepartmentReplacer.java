package com.endava.service.replacer;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import com.endava.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

@Component
@RequiredArgsConstructor
public class DepartmentReplacer implements Replacer<Department, DepartmentDTO> {

    private final EmployeeService service;

    @Override
    public void replaceValues(Department department, DepartmentDTO departmentDTO) {
        Employee manager = null;

        if(nonNull(departmentDTO.getManagerEmail())){
            manager = service.findEmployeeByEmail(departmentDTO.getManagerEmail());
        }

        department.setManager(manager);

    }
}
