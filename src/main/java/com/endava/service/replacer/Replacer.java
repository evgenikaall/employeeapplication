package com.endava.service.replacer;

public interface Replacer<S, T> {
    public void replaceValues(S replaceEntity, T dataForReplace);
}
