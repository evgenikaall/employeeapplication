package com.endava.service.replacer;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.dto.EmployeeDTO;
import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import com.endava.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Component
public class EmployeeReplacer implements Replacer<Employee, EmployeeDTO> {

    @Lazy
    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private Converter<Department, DepartmentDTO> toDTO;

    @Override
    public void replaceValues(Employee replaceEntity, EmployeeDTO dataForReplace) {


        replaceEntity.setFirstName(dataForReplace.getFirstName());
        replaceEntity.setLastName(dataForReplace.getLastName());
        replaceEntity.setSalary(dataForReplace.getSalary());
        replaceEntity.setHireDate(dataForReplace.getHireDate());

        Department workingIn = null;

        if(nonNull(dataForReplace.getDepartment())){
            workingIn = departmentService.findDepartmentByName(dataForReplace.getDepartment().getName());
        }

        replaceEntity.setDepartment(workingIn);

        Department managedDepartment = null;

        if(isNull(dataForReplace.getManagedDepartment()) && nonNull(replaceEntity.getManagedDepartment())){
            managedDepartment = replaceEntity.getManagedDepartment();
            managedDepartment.setManager(null);
            departmentService.updateDepartment(toDTO.convert(managedDepartment));
            managedDepartment = null;
        }

        if(nonNull(dataForReplace.getManagedDepartment())){
            managedDepartment = departmentService.findDepartmentByName(dataForReplace.getManagedDepartment().getName());
            managedDepartment.setManager(replaceEntity);
        }

        replaceEntity.setManagedDepartment(managedDepartment);
    }
}
