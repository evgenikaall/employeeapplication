package com.endava.service.converter;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.entity.Department;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

@Component
public class DepartmentToDTO implements Converter<Department, DepartmentDTO> {
    @Override
    public DepartmentDTO convert(Department department) {
        String email = null;
        if(nonNull(department.getManager())) email = department.getManager().getEmail();
        return DepartmentDTO.builder()
                .name(department.getName())
                .managerEmail(email)
                .build();
    }
}
