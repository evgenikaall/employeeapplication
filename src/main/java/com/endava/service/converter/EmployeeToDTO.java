package com.endava.service.converter;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.dto.EmployeeDTO;
import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.Objects.nonNull;

@Component
@RequiredArgsConstructor
public class EmployeeToDTO implements Converter<Employee, EmployeeDTO> {

    private final Converter<Department, DepartmentDTO> toDTO;

    @Override
    public EmployeeDTO convert(Employee employee) {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .email(employee.getEmail())
                .job(employee.getJob())
                .salary(employee.getSalary())
                .hireDate(employee.getHireDate())
                .build();

        if(nonNull(employee.getDepartment()))
            employeeDTO.setDepartment(toDTO.convert(employee.getDepartment()));

        if(nonNull(employee.getManagedDepartment()))
            employeeDTO.setManagedDepartment(toDTO.convert(employee.getManagedDepartment()));

        return employeeDTO;
    }
}
