package com.endava.service.converter;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import com.endava.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.Objects.nonNull;

@Component
@RequiredArgsConstructor
public class DepartmentDTOToDepartment implements Converter<DepartmentDTO, Department> {

    private final EmployeeService employeeService;

    @Override
    public Department convert(DepartmentDTO departmentDTO) {
        Employee manager = null;
        if(nonNull(departmentDTO.getManagerEmail()))
            employeeService.findEmployeeByEmail(departmentDTO.getManagerEmail());

        return Department.builder()
                .name(departmentDTO.getName())
                .manager(manager)
                .build();
    }
}
