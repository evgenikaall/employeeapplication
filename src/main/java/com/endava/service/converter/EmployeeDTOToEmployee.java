package com.endava.service.converter;

import com.endava.model.dto.EmployeeDTO;
import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import com.endava.service.DepartmentService;
import com.endava.util.exception.DepartmentsManagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.Objects.nonNull;


@Component
public class EmployeeDTOToEmployee implements Converter<EmployeeDTO, Employee> {

    @Lazy
    @Autowired
    private DepartmentService service;

    @Override
    public Employee convert(EmployeeDTO employeeDTO) {

        Employee employee = Employee.builder()
                .firstName(employeeDTO.getFirstName())
                .lastName(employeeDTO.getLastName())
                .email(employeeDTO.getEmail())
                .job(employeeDTO.getJob())
                .salary(employeeDTO.getSalary())
                .hireDate(employeeDTO.getHireDate())
                .build();

        Department department = null;

        if (nonNull(employeeDTO.getDepartment())) department =
                department = service.findDepartmentByName(employeeDTO.getDepartment().getName());

        Department managedDepartment = null;

        if (nonNull(employeeDTO.getManagedDepartment())) {
            managedDepartment = service.findDepartmentByName(employeeDTO.getManagedDepartment().getName());

            if (nonNull(managedDepartment.getManager()))
                throw new DepartmentsManagingException("This department is already managed");
            else
                managedDepartment.setManager(employee);
        }

        employee.setDepartment(department);
        employee.setManagedDepartment(managedDepartment);

        return employee;
    }
}
