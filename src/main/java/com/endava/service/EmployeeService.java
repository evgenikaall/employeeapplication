package com.endava.service;

import com.endava.model.dto.EmployeeDTO;
import com.endava.model.entity.Employee;
import com.endava.repository.jpa.EmployeeJpaRepository;
import com.endava.service.replacer.Replacer;
import com.endava.util.exception.EmployeeNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final Converter<Employee, EmployeeDTO> toDTO;
    private final Converter<EmployeeDTO, Employee> toEntity;
    private final Replacer<Employee, EmployeeDTO> replacer;
    private final EmployeeJpaRepository repository;


    public List<EmployeeDTO> findAll(){
        return repository.findAll().stream().map(toDTO::convert).collect(toList());
    }

    public Employee findEmployeeByEmail(String email) {
        return repository.findEmployeeByEmail(email)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with current email not exist"));
    }

    public EmployeeDTO save(EmployeeDTO employeeDTO){
        return toDTO.convert(repository.save(requireNonNull(toEntity.convert(employeeDTO))));
    }

    public EmployeeDTO findEmployeeDTOByEmail(String email) {
        return toDTO.convert(findEmployeeByEmail(email));
    }

    private void deleteEmployeeById(Long id){
        repository.deleteById(id);
    }

    public void deleteEmployeeByEmail(String email){
        final Employee employeeForDeleting = findEmployeeByEmail(email);
        deleteEmployeeById(employeeForDeleting.getId());
    }

    public EmployeeDTO update(EmployeeDTO employeeDTO){
        Employee employee = findEmployeeByEmail(employeeDTO.getEmail());
        replacer.replaceValues(employee, employeeDTO);
        return toDTO.convert(repository.save(employee));
    }
}
