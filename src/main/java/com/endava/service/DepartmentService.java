package com.endava.service;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.entity.Department;
import com.endava.repository.jpa.DepartmentJpaRepository;
import com.endava.service.replacer.Replacer;
import com.endava.util.exception.DepartmentNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class DepartmentService {

    private final DepartmentJpaRepository repository;
    private final Converter<Department, DepartmentDTO> toDTO;
    private final Converter<DepartmentDTO, Department> toEntity;
    private final Replacer<Department, DepartmentDTO> replacer;

    public List<DepartmentDTO> findAllDepartments(){
        return repository.findAll().stream().map(toDTO::convert).collect(toList());
    }

    // TODO UNIQUE DEP NAME
    public DepartmentDTO saveDepartment(DepartmentDTO departmentDTO){
        if(isNull(departmentDTO)) throw new IllegalArgumentException("Empty request body");
        return toDTO.convert(repository.save(requireNonNull(toEntity.convert(departmentDTO))));
    }

    public DepartmentDTO updateDepartment(DepartmentDTO departmentDTO){
        Department departmentByName = findDepartmentByName(departmentDTO.getName());
        replacer.replaceValues(departmentByName, departmentDTO);
        return toDTO.convert(repository.save(departmentByName));
    }

    public void deleteDepartmentByName(String name){
        Department department = findDepartmentByName(name);
        repository.deleteById(department.getId());
    }

    public DepartmentDTO findDepartmentDTOByName(String name){
        return toDTO.convert(findDepartmentByName(name));
    }

    public Department findDepartmentByName(String name) {
        return repository.findDepartmentByName(name)
                .orElseThrow(() -> new DepartmentNotFoundException("Department with this name not found"));
    }

    public Department findDepartmentByManagerEmail(String email){
        return repository.findDepartmentByManagerEmail(email)
                .orElseThrow(() -> new DepartmentNotFoundException("Department with manager with current email not exist"));
    }

}
