INSERT INTO department
     VALUES (10, 'IT_DEP', null ),
            (20, 'BC_DEP', null );

INSERT INTO employee
     VALUES (1, 'John', 'Sins', 'BAKER','johnS@gmail.com', 1000, '1999-01-08 04:05:06', 10);

UPDATE department
SET manager_id = 1
WHERE department_id = 20;